const express = require('express')
const app = express()
const uuid = require('uuid/v4')
const session = require('express-session')
const FileStore = require('session-file-store')(session)
const bodyParser = require("body-parser")
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy


const constants = require('./constants.js')

const User = require('./User.js')
user = new User()

const Car = require('./Car.js')
car = new Car()

const Ride = require('./Ride.js')
ride = new Ride()

var router = express.Router();

const create_response = (status = consts.DEFAULT_ERROR_STATUS, content = null) => {
  return { status: status, content: content }
}

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(passport.initialize());
app.use(passport.session());
app.use(session({
  genid: (req) => {
    console.log('Inside the session middleware')
    console.log(req.sessionID)
    return uuid() // use UUIDs for session IDs
  },
  store: new FileStore(),
  secret: 'a protected secret', //to random with env vars in prod
  resave: false,
  saveUninitialized: true
}))
app.use(router);

const users = [
  { id: 1, mail: 'bouvet.romain18@gmail.com', password: 'password' }
]

// configure passport.js to use the local strategy
passport.use(new LocalStrategy(
  (mail, password, done) => {
    console.log('Inside local strategy callback')
    // here is where you make a call to the database
    // to find the user based on their username or email address
    // for now, we'll just pretend we found that it was users[0]
    const user = users[0]
    if (mail === user.mail && password === user.password) {
      console.log('Local strategy returned true')
      return done(null, user)
    }
  }
));

// tell passport how to serialize the user
passport.serializeUser((user, done) => {
  console.log('Inside serializeUser callback. User id is save to the session file store here')
  done(null, user.id);
});

app.get('/', (req, res) => {
  console.log('Inside the homepage callback function')
  console.log(req.sessionID)
  res.send('Hit the home page !')
})

router.route('/login')
  .get((req, res) => {
    console.log('Inside GET /login callback function')
    console.log(req.sessionID)
    res.send(`You got the login page!\n`)
  })
  .post((req, res, next) => {
    console.log('Inside POST /login callback')
    passport.authenticate('local', (err, user, info) => {
      console.log('Inside passport.authenticate() callback');
      console.log(`req.session.passport: ${JSON.stringify(req.session.passport)}`)
      console.log(`req.user: ${JSON.stringify(req.user)}`)
      req.login(user, (err) => {
        console.log('Inside req.login() callback')
        console.log(`req.session.passport: ${JSON.stringify(req.session.passport)}`)
        console.log(`req.user: ${JSON.stringify(req.user)}`)
        return res.send('You were authenticated & logged in!\n');
      })
    })(req, res, next);
  })

router.route('/users')
  .post((req, res) => {
    user.create(req.body, () => {
      res.json(create_response(constants.SUCCESS_STATUS, 'Created user'))
    })
  })
  .get((_, res) => {
    user.get_all((users) => {
      res.json(create_response(constants.SUCCESS_STATUS, users))
    })
  })

router.route('/users/:id')
  .get((req, res) => {
    user.get(req.params.id, (user) => {
      res.json(create_response(constants.SUCCESS_STATUS, user))
    })
  })
  .put((req, res) => {
    user.update(req.params.id, req.body, () => {
      res.json(create_response(constants.SUCCESS_STATUS, 'Modified user n' + req.params.id))
    })
  })
  .delete((req, res) => {
    user.delete(req.params.id, () => {
      res.json(create_response(constants.SUCCESS_STATUS, 'Deleted user n' + req.params.id))
    })
  })

router.route('/users/:id/cars')
  .post((req, res) => {
    car.create(req.body, req.params.id, () => {
      res.json(create_response(constants.SUCCESS_STATUS, 'Created car'))
    })
  })

  .get((_, res) => {
    car.get_all((cars) => {
      res.json(create_response(constants.SUCCESS_STATUS, cars))
    })
  })

router.routes('/users/:id/rides')
  .get((req, res) => {
    ride.my_rides(req.params.id, (rides) => {
      res.json(create_response(constants.SUCCESS_STATUS, rides))
    })
  })

router.routes('/searchrides')
  .get((req, res) => {
    ride.get_all((rides) => {
      res.json(create_response(constants.SUCCESS_STATUS, rides))
    })
  })

router.routes('/rides/:id')
  .get((req, res) => {
    ride.get(req.params.id, (ride) => {
      res.json(create_response(constants.SUCCESS_STATUS, ride))
    })
  })

const PORT = 1500
app.listen(PORT, () => {
  console.log("Server available on port " + PORT)
})
