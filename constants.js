module.exports = Object.freeze({
    HOST: 'localhost',
    USER: 'root',
    PASSWORD: '',
    DATABASE: 'blablavoiture',

    SUCCESS_STATUS: 200,
    DEFAULT_ERROR_STATUS: 500,
    UNAUTHORIZED_STATUS: 401,
    RESOURCE_NOT_FOUND_STATUS: 404
})