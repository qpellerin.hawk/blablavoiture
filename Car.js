const Database = require('./Database.js');
db = (new Database()).db;

class Car{
    create(body, id_user, res) {
        db.query(
        "INSERT INTO cars(numberplate, model, brand, id_user) VALUES(?,?,?,?)",
        [
            body.numberplate,
            body.model,
            body.brand,
            id_user
        ],
        (err, rows) => {
            if (err) {
                console.log(err)
                throw err
            }

            res(rows)
        })
    }

    get_all() {
        db.query("SELECT * FROM cars WHERE id_user=?", id, (err, rows) => {
            if (err) {
                console.log(err)
                throw err
            }
            res(rows)
        })
    }
}

module.exports = Car
