const Database = require('./Database.js');
db = (new Database()).db;

class Ride {
    get(id, res) {
        db.query("SELECT * FROM rides WHERE id=?", id, (err, rows) => {
            if (err) {
                console.log(err)
                throw err
            }
            res(rows[0])
        })
    }

    get_all(req, res) {
        // Get rides WHERE X AND Y AND Z AND AA AND BB AND CC
    }

    my_rides(id, res) {
        db.query("SELECT * FROM rides WHERE id = (SELECT r.id, rp.id_ride FROM rides r, rides_passengers rp WHERE r.id_driver=? AND rp.id_passenger=?)", id, (err, rows) => {
            if (err) {
                console.log(err)
                throw err
            }
            res(rows)
        })
    }
}

module.exports = Ride
