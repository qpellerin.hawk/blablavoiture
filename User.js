const crypto = require('crypto')
const Database = require('./Database.js');
db = (new Database()).db;

const hash_password = (password) => {
  const sha256 = crypto.createHash('sha256')
  const hash = sha256.update(password).digest('base64')
  return hash
}

class User {
  create(body, res) {
    db.query(
      "INSERT INTO users(name, first_name, mail, password, phone) VALUES(?,?,?,?,?)",
      [
        body.name,
        body.first_name,
        body.mail,
        hash_password(body.password),
        body.phone
      ],
      (err, rows) => {
        if (err) {
          console.log(err)
          throw err
        }

        res(rows)
      })
  }

  get_all(res) {
    db.query("SELECT * FROM users", (err, rows) => {
      if (err) {
        console.log(err)
        throw err
      }

      res(rows)
    })
  }

  get(id, res){
    db.query("SELECT * FROM users WHERE id=?", id, (err, rows) => {
      if (err) {
        console.log(err)
        throw err
      }

      res(rows[0])
    })
  }

  update(id, body, res){
    db.query(
      "UPDATE users SET name=?, first_name=?, mail=?, phone=? WHERE id=?",
      [
        body.name,
        body.first_name,
        body.mail,
        body.phone,
        id
      ],
      (err, rows) => {
        if (err) {
          console.log(err)
          throw err
        }

        res(rows[0])
      })
  }

  delete(id, res){
    db.query("DELETE FROM users WHERE id=?", id, (err, _) => {
      if (err) {
        console.log(err)
        throw err
      }

      res()
    })
  }
}

module.exports = User;