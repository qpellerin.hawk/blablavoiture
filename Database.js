
const mysql = require('mysql')

class Database{
    constructor(){
        this.db=mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'blablavoiture'
        });

        this.db.connect();
    }
}

module.exports = Database;
